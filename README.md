# Data Science Curriculum

## Table of Contents
- [Foundations](#foundations)
- [Computation Statistics and Analysis](#computation-statistics-and-analysis)
- [Machine Learning and Artificial Intelligence](#machine-learning-and-artificial-intelligence)
- [Deep Learning and Neural Networks](#deep-learning-and-neural-networks)
- [Natural Language Processing](#natural-language-processing)
- [Data Mining and Visualization](#data-mining-and-visualization)
- [Other Resources](#other-resources)
- [References](#references)

### Foundations
Course | Institute | Length
:-- | :--: | :--:
[10-606/607 Math Background for ML](https://www.youtube.com/playlist?list=PL7y-1rk2cCsAqRtWoZ95z-GMcecVG5mzA) | CMU | 25 hours 📹
[Stat 2.1x: Descriptive Statistics](https://www.youtube.com/playlist?list=PL_Ig1a5kxu56TfFnGlRlH2YpOBWGiYsQD) | Berkeley | 10 hours 📹
[Stat 2.2x: Probability](https://www.youtube.com/playlist?list=PL_Ig1a5kxu57qPZnHm-ie-D7vs9g7U-Cl) | Berkeley | 10 hours 📹
[Stat 2.3x: Inference](https://www.youtube.com/playlist?list=PL_Ig1a5kxu5666P35bQ0AQqupnbvzLv-x) | Berkeley | 10 hours 📹
[Data 8: The Foundations of Data Science](http://data8.org/su17/) | Berkeley | 40 hours 📹
[Data 100: Principles and Techniques of Data Science](http://www.ds100.org/fa17/) | Berkeley | 16 weeks
[DS-GA-1001 Introduction to Data Science](https://github.com/briandalessandro/DataScienceCourse/blob/master/ipython/references/Syllabus_2017.pdf) | NYU | 10 weeks

### Computational Statistics and Analysis
Course | Institute | Length
:-- | :--: | :--:
[6.008x Computational Probability and Inference](https://www.edx.org/course/computational-probability-inference-mitx-6-008-1x) | MIT | 48 hours 📹
[DS-GA-1003 Machine Learning & Computational Statistics](https://www.youtube.com/playlist?list=PL_Ig1a5kxu557b_UPWNsCfT_XOcNnWJxL) | NYU | 22 hours 📹
[DS-GA-1005 Inference and Representation](https://inf16nyu.github.io/home/) | NYU | 15 weeks
[STA302 Methods of Data Analysis I](http://www.utstat.utoronto.ca/alisong/Teaching/1112/Sta302/sta302.html) | UToronto | 12 weeks
[STA303 Methods of Data Analysis II](http://www.utstat.utoronto.ca/alisong/Teaching/1112/Sta303/sta303.html) | UToronto | 12 weeks

### Machine Learning and Artificial Intelligence
Course | Institute | Length
:-- | :--: | :--:
[10-601 Machine Learning](https://www.cs.cmu.edu/~roni/10601/) | CMU | 40 hours 📹
[10-702 Statistical Machine Learning](https://www.youtube.com/playlist?list=PLjbUi5mgii6B7A0nM74zHTOVQtTC9DaCv) | CMU | 30 hours 📹
[15-780 Graduate Artificial Intelligence](https://www.youtube.com/playlist?list=PLpIxOj-HnDsPfw9slkk0BfwuiNEYVnsd_) | CMU | 40 hours 📹
[CS188 Artifical Intelligence](https://www.youtube.com/playlist?list=PL_Ig1a5kxu57_NyXZWdJrM4JNGDNeABir) | Berkeley | 30 hours 📹
[CS189 Machine Learning](www.eecs189.org) | Berkeley | 12 weeks
[CS205A Mathematical Methods for Robotics, Vision, & Graphics](https://www.youtube.com/playlist?list=PL_Ig1a5kxu54U8Ip7q8wDpn9wbPaNATno) | Stanford | 25 hours 📹
[CS223A Introduction to Robotics](https://www.youtube.com/playlist?list=PL65CC0384A1798ADF) | Stanford | 18 hours 📹
[6.034 Artificial Intelligence](https://www.youtube.com/playlist?list=PLUl4u3cNGP63gFHB6xb-kVBiQHYe_4hSi) | MIT | 28 hours 📹

### Deep Learning and Neural Networks
Course | Institute | Length
:-- | :--: | :--:
[6.S191 Introduction to Deep Learning](https://www.youtube.com/playlist?list=PLkkuNyzb8LmxFutYuPA7B4oiMn6cjD6Rs) | MIT | 4 hours 📹
[10-707 Topics in Deep Learning](https://www.youtube.com/playlist?list=PLpIxOj-HnDsOSL__Buy7_UEVQkyfhHapa) | CMU | 22 hours 📹
[DS-GA-1008 Deep Learning](https://cilvr.nyu.edu/doku.php?id=courses:deeplearning2017:start) | NYU | 11 weeks
[Neural Networks for Machine Learning](https://www.youtube.com/playlist?list=PLoRl3Ht4JOcdU872GhiYWf6jwrk_SNhz9) | Geoffrey Hinton | 20 hours 📹
[Neural Networks and Deep Learning](https://www.youtube.com/playlist?list=PLkDaE6sCZn6Ec-XTbcX1uRg2_u4xOEky0) | deeplearning.ai | 8 hours 📹
[Improving Deep Neural Networks](https://www.youtube.com/playlist?list=PLkDaE6sCZn6Hn0vK8co82zjQtt3T2Nkqc) | deeplearning.ai | 6 hours 📹
[Structuring Machine Learning Projects](https://www.youtube.com/playlist?list=PLkDaE6sCZn6E7jZ9sN_xHwSHOdjUxUW_b) | deeplearning.ai | 5 hours 📹
[Convolutional Neural Networks](https://www.youtube.com/playlist?list=PLkDaE6sCZn6Gl29AoE31iwdVwSG-KnDzF) | deeplearning.ai | 7 hours 📹
[CS231n Convolutional Neural Networks for Visual Recognition](https://www.youtube.com/playlist?list=PL3FW7Lu3i5JvHM8ljYj-zLfQRF3EO8sYv) | Stanford | 20 hours 📹

### Natural Language Processing
Course | Institute | Length
:-- | :--: | :--:
[CS224n Natural Language Processing with Deep Learning](https://www.youtube.com/playlist?list=PL3FW7Lu3i5Jsnh1rnUwq_TcylNr7EkRe6) | Stanford | 22 hours 📹
[CS288 Statistical Natural Language Processing](https://people.eecs.berkeley.edu/~klein/cs288/fa14/) | Berkeley | 15 weeks
[L&S88-5 Rediscovering Texts as Data](https://github.com/henchc/Rediscovering-Text-as-Data) | Berkeley | 13 weeks
[DS-GA-1011 Natural Language Processing with Representation Learning](https://docs.google.com/document/d/1SIPSt4aeB3Lys9ztCp47Y4v68R6Awt8NBTlCObp2njg/edit) | NYU | 14 weeks
[DS-GA-1012 Natural Language Understanding and Computational Semantics](https://docs.google.com/document/d/1mkB6KA7KuzNeoc9jW3mfOthv_6Uberxs8l2H7BmJdzg/edit) | NYU | 14 weeks
[DS-GA-3001 Text as Data](https://docs.google.com/document/d/1YS5QRvqMJVs9n3sK5fFjuldY7_vh42C5uUfxUGgL-Gc/edit) | NYU | 14 weeks

### Data Mining and Visualization
Course | Institute | Length
:-- | :--: | :--:
[10-605 Machine Learning with Large Datasets](https://www.youtube.com/playlist?list=PL2rOgfQmbt2xdTbpc1gvDNB1p5MTHTuzj) | CMU | 30 hours 📹
[15-721 Advanced Database Systems](https://www.youtube.com/playlist?list=PLSE8ODhjZXjbisIGOepfnlbfxeH7TW-8O) | CMU | 28 hours 📹
[DS-GA-1004 Big Data](https://www.vistrails.org/index.php/Course:_Big_Data_2016) | NYU | 15 weeks
[CS171 Visualization](https://www.youtube.com/playlist?list=PL_Ig1a5kxu54jM-q7ucZkhKxlA6IRtKlQ) | Harvard | 24 hours 📹
[CS284r Social Data Mining](people.seas.harvard.edu/~yaron/SocialDataMining/index.html) | Harvard | 10 weeks
[CS229r Algorithms for Big Data](https://www.youtube.com/playlist?list=PL2SOU6wwxB0v1kQTpqpuu5kEJo2i-iUyf) | Stanford | 30 hours 📹
[CS246 Mining Massive Datasets](https://www.youtube.com/playlist?list=PL_Ig1a5kxu56sFkL5ksOV2XgrfcZ3U2qA) | Stanford | 24 hours 📹
[CSC411 Machine Learning and Data Mining](http://www.cs.toronto.edu/~guerzhoy/411/) | UToronto | 12 weeks
[CSC443 Database System Technology](http://www.teach.cs.toronto.edu/~csc443h/winter/#lectures-link) | UToronto | 10 weeks
[STA414 Statistical Methods for Machine Learning and Data Mining](https://duvenaud.github.io/sta414/) | UToronto | 11 weeks

### Other Resourses
Title | Source
:-- | :--:
Probability for Data Science | [Gitbook](https://www.gitbook.com/book/probability/probability-for-data-science) 📚
Neural Networks and Deep Learning | [Gitbook](http://neuralnetworksanddeeplearning.com/index.html) 📚
Deep Learning Papers | [GitHub](https://github.com/songrotek/Deep-Learning-Papers-Reading-Roadmap) 📄
Deep Learning | [MIT Press](http://www.deeplearningbook.org/) 📚
Neural Networks | [Université de Sherbrooke](http://info.usherbrooke.ca/hlarochelle/neural_networks/content.html)
Reinforcement Learning | [UCL](http://www0.cs.ucl.ac.uk/staff/d.silver/web/Teaching.html)

### References
* [NYU Data Science Curriculum](https://cds.nyu.edu/academics/ms-in-data-science/ms-curriculum/)
* [Berkeley Data Science Courses](https://data.berkeley.edu/education/courses)
* [CMU Machine Learning Classes](https://www.ml.cmu.edu/academics/classes.html)
* [Harvard Data Science Courses](https://www.seas.harvard.edu/programs/graduate/applied-computation/cse-courses)
* [Stanford Data Science Courses](https://statistics.stanford.edu/academics/ms-statistics-data-science)
* [UToronto Data Science Courses](http://www.cs.toronto.edu/~guerzhoy/datasci_advice/courses/)
